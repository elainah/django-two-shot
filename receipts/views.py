from django.shortcuts import render, get_list_or_404, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptForm, CreateCatForm, CreateAcctForm

# Create your views here.
@login_required
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt,
    }
    return render(request, "receipts/listview.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "receipts/categories/category_list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accounts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCatForm(request.POST)
        if form.is_valid():
            categories = form.save(False)
            categories.owner = request.user
            categories.save()
            return redirect("category_list")
    else:
        form = CreateCatForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAcctForm(request.POST)
        if form.is_valid():
            accounts = form.save(False)
            accounts.owner = request.user
            accounts.save()
            return redirect("account_list")
    else:
        form = CreateAcctForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/accounts/create.html", context)
